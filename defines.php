<?php
/* $Id: defines.php,v 0.0.0.1 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Global Configuration File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/




/*
 *----------------------------------------------
 * ====> DO NOT EDIT BELOW <====
 *----------------------------------------------
*/

/* Catch the GET and POST variables
*/

$c = 0;
foreach($_GET as $key => $value) {
	if ($c == 0) $firstkey = $key; 								// Taking the key name from the first element of _POST array
	if ($c == 0) $firstvalue = $value; 							// Taking the key value from the first element of _POST array
    if ($c >= 2 && "3" >= strlen($key)) $ids .= ",".$key; 		// Taking the key names from the third element of _POST array
    if ($c >= 2) $idsc .= "���".$key."#".$value;			 	// Taking the key names and values from the third element of _POST array
	/*print "KEY:$key :: VALUE:$value :: C:$c :: IDS:$ids<br />";						// DEBUG string */
 	$$key = $value;												// Create the global variable from the key
	$c++;
}

$c = 0;
foreach($_POST as $key => $value) {
	if ($c == 0 && empty($firstkey)) $firstkey = $key; 								// Taking the key name from the first element of _POST array
	if ($c == 0 && empty($firstvalue)) $firstvalue = $value; 							// Taking the key value from the first element of _POST array
    if ($c >= 2 && "3" >= strlen($key)) $ids .= ",".$key; 		// Taking the key names from the third element of _POST array
    if ($c >= 2) $idsc .= "���".$key."#".$value;			 	// Taking the key names and values from the third element of _POST array
	/*print "KEY:$key :: VALUE:$value :: C:$c :: IDS:$ids<br />";						// DEBUG string */
 	if (empty($$key)) $$key = $value;												// Create the global variable from the key
	$c++;
}
flush($c);

/* Creating Global Variables
*/
if (empty($sort)) $sort = "id";
$path = str_replace(basename($_SERVER[SCRIPT_NAME]), "", $_SERVER[SCRIPT_NAME]); // PHP5
$baselink = "http://" . $_SERVER['HTTP_HOST'] . str_replace("index.php", "", str_replace("actions/", "", $_SERVER['PHP_SELF']));
//$path = $_SERVER['DOCUMENT_ROOT'] . $_SERVER['REDIRECT_URL']."/"; // PHP4
/*
###########
## Languages ##
##########
*/

/* Check if had been choosed a language
*/ 
if (empty($klang)) $klang = "italiano";

/* Catch the available languages

$dh=opendir($path . "languages");

while ($file=readdir($dh)) {
		$filep = explode(".", $file);
		$filename = $filep[0];
		$filext = array_pop($filep);
		if ($filext == "kml" && $filename == $klang) include "$path$file";
}
*/
/*
############
## Enviroments ##
###########
*/
function getmicrotime()
{
         list($usec, $sec) = explode(" ",microtime());
         return ((float)$usec + (float)$sec);
}

//$ci = array_pop(explode("\\", $_SERVER[PATH_TRANSLATED]));
//$basedir = str_replace($ci, "", $_SERVER[PATH_TRANSLATED]);

if (!empty($_FILES['file']['name'])) {
		$uploaddir = $basedir . "uploads/";
		if ($admin == "sponsor") $uploaddir = $uploaddir . "sponsor/" . $sid . "/";
		if ($admin == "galleria") $uploaddir = $uploaddir . "gallery/" . $sid . "/";

		$uploadfile = $uploaddir . basename($_FILES['file']['name']);
		$tmpsize = $_FILES['file']['size'];
		$tmpext  = strtolower(array_pop(explode('.', $_FILES['file']['name'])));
		mkdir($uploaddir, 0777);

		move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
}
?>