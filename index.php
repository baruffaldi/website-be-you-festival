<?php
/* $Id: index.php,v 0.0.0.1 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Index Script
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

include 'includes.php';

$SQLStream = BEYOU_SQL::opendb($mdb);
$time_start = getmicrotime(); // Timing rendering page
if (isset($_GET[admin])) BEYOU_ACTIONS::reglogin();
?>
<!--
$Id: index.php,v 0.0.0.1 07/06/2006 02:02:07 mdb Exp $
$Author: mdb $

www.be-you.org Index Script

Copyright Kimera Team (c) 2006

You may not reproduce it elsewhere without the prior written permission of the author.
However, feel free to study the code and use techniques you learn from it elsewhere.
-->

<?php print "<?xml version='1.0' encoding='iso-8859-1' ?>"; ?>

<!DOCTYPE html
         PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
         'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>

<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>

         <head>
                  <title><?php print $headache['title']; ?></title>

<?php foreach($headache as $metaname => $metacontent) {?>
				  <meta name='<?php print $metaname; ?>' content='<?php print $metacontent; ?>' />
<?php } ?>
<?php foreach($headmeta as $metahttp => $metacontent) {?> 
				  <meta http-equiv='<?php print $metahttp; ?>' content='<?php print $metacontent; ?>' />
<?php } if (!isset($action) && !isset($page)) print "<meta http-equiv='Refresh' content='20;url=\"\"'>";?>
<?php foreach($headlinks as $rel => $hreftype) { $divide = explode("###", $hreftype); ?> 
				  <link rel='<?php print $rel; ?>' href='<?php if (empty($firstkey) && $rel == "stylesheet") { print "css/intro.css"; } elseif ($rel == "stylesheet" && $firstkey == "admin") { print "css/admin.css"; } else { print $divide[0]; }?>' type='<?php print $divide[1]; ?>' />
<?php flush($divide); } if (!isset($_GET[admin])) { ?>
				  <script type='text/javascript'> 
					function apri(url) { 
							newin = window.open(url,'titolo','scrollbars=yes,resizable=no, width=960,height=768,status=no,location=no,toolbar=no');
					} 
				  </script>
<?php } else { ?>
				  <script type='text/javascript'> 
					function apri(url) { 
							newin = window.open(url,'titolo','scrollbars=yes,resizable=no, width=960,height=600,status=no,location=no,toolbar=no');
					}
				  </script>
<?php } if (isset($admin)) { ?>
				  <script type="text/javascript" src="editor.js"></script> <? } ?>
         </head>

         <body <?php print $body; ?>>
<?php 
        BEYOU_LAYOUT::route_section($firstkey, $firstvalue);
?>
         </body>

</html>

<?php BEYOU_SQL::closedb($mdb, ""); ?>