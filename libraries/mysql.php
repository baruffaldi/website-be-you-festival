<?php
/* $Id: mysql.php,v 0.0.0.7 22/11/2004 18:02:07 mdb Exp $
 * $Author: mdb $
 *
* www.be-you.org MySQL Mini-FrameWork
 * php released under PHP License - http://www.php.net/license/3_0.txt
 *
 * Light version for commercial purpose only
 * 
 *
 * Copyright (c) 2004 Filippo Baruffaldi <mdb AT insaneminds DOT org> <http://www.insaneminds.org>
 * All associated graphics Copyright (c) 2004 Filippo Baruffaldi
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

include 'conf.php';

class BEYOU_SQL {
   function create() 
   {

	    // Figure out which connections are open, automatically opening any connections
	    // which are failed or not yet opened but can be (re)established.
		$con = mysql_pconnect(MHOST, MUSER, MPASS);
		if (!($con === false)) {
		    if (mysql_select_db(MDB, $con) === false) {
			   echo('Could not select database: ' . mysql_error());
			   
		    }
		}
		   
	    // Return the connection.
	    return $con;
   }

	function opendb()
	{
        $str = mysql_connect(MHOST, MUSER, MPASS)
           or print("Sorry! Can't connect on database<br />Error: " . mysql_error());
		    if (mysql_select_db(MDB, $str) === false) {
			   echo('Could not select database: ' . mysql_error());
			   
		    }
			
        return $str;
	}

	function closedb($str, $result)
	{
       if ($result != "") @mysql_free_result($result);
       @mysql_close($str);
	}

	function select($table, $col, $where, $limit, $order)
	{
       $table = explode('?', $table);
	   if ($table[0] == "by_news" && !isset($_GET[admin])) {
		       if (!empty($limit)) {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
		       } else {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
		       }
		} elseif ($table[0] == "by_gallery" && !isset($_GET[admin])) {
		       if (!empty($limit)) {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
		       } else {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
		       }
		} else {
		       if (!empty($limit)) {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order ASC LIMIT $limit";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC LIMIT $limit";
		       } else {
		             $query = "SELECT $col FROM $table[0] ORDER BY $order ASC";
		             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC";
		       }
		}
		
       $result = mysql_query($query)
           or print("Sorry! Can`t work that query[select] on database<br />Error: " . mysql_error() . "<br />");
		   
       if ($table[1] == 'debug') print $query;
       return $result;
	}

	function insertrow($table, $col, $values)
	{
       $table = explode('?', $table);
       $query = "INSERT INTO $table[0] ($col) VALUES ($values)";

       $result = mysql_query($query)
           or print('Sorry! Can`t work that query[insert] on database<br />Error: ' . mysql_error() . "<br />");
		   
       if ($table[1] == 'debug') print $query;
       return $result;
	}

	function deleterow($table, $where)
	{
       $table = explode('?', $table);
       $query = "DELETE FROM $table[0] WHERE $where";

       $result = mysql_query($query)
           or print('Sorry! Can`t work that query[delete] on database<br />Error: ' . mysql_error() . "<br />");
		   
       if ($table[1] == 'debug') print $query;
       return $result;
	}

	function printresult($result) {
	while ($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
		foreach ($linea as $valore_colonna) {
			$return .= "$valore_colonna<br />\n";
		}
	}
	return $return;
	}

	function printresulttb($result) {
	while ($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
		$return = "\t<tr>\n";
		foreach ($linea as $valore_colonna) {
			$return .= "\t\t<td>$valore_colonna</td>\n";
		}
		$return .= "\t</tr>\n";
	}
	return $return;
	}

	function modifyrow($table, $col, $value, $where)
	{
		$table = explode('?', $table);
		$query = "UPDATE $table[0] SET $col = ".'"'.$value.'"'." WHERE $where";

		$result = mysql_query($query)
			or print('Sorry! Can`t work that query[update/set] on database<br />Error: ' . mysql_error() . "<br />");
		   
       if ($table[1] == 'debug') print $query;
		return $result;
	}

	function insane_sql_query($query)
	{
		$querya = explode('?', $query);
		$result = mysql_query($querya[0])
			or print('Sorry! Can`t work that query[insane] on database<br />Error: ' . mysql_error() . "<br />");
		   
        if ($table[1] == 'debug') print $query;
	   
		return $result;
	}

}
?>
