<?php
/* $Id: headers.php,v 0.0.0.1 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
* www.be-you.org Configuration File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/
if (empty($firstkey)) $firstkey = "xhtml";

$headache = Array
(
   'title' => "Be-You :: Young Festival",
   'subject' => "Festival dell` arte in Liguria",
   'metadata' => "Be-You",
   'author' => "Filippo Baruffaldi, Patrizio Belvedere",
   'publisher' => "www.kimera-lab.com",
   'date' => "07Jun2006",
   'form' => "xhtml"
);

$headmeta = Array
(
   'Keywords' => "Be-you, Festival, Liguria, Genova",
   'Description' => "Festival dell` arte in Liguria",
   'Content-Type' => "text/xhtml; charset=iso-8859-1"
);


$stile = explode(";", $_SERVER[HTTP_USER_AGENT]);
$test = explode(" ", $stile[1]);
$ou = explode('(', $_SERVER[HTTP_USER_AGENT]);
$ouz = explode(';', $ou[1]);
$stilez = explode(')', $_SERVER[HTTP_USER_AGENT]);
$testz = explode("/", array_pop($stilez));

switch($ouz[0]) {
		default:
			if ($test[1] == "MSIE") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleIE.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} elseif ($test[1] == "U") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleFF.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} else {
				$headlinks = Array
				(
				   'stylesheet' => "css/style.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			}
			break;
			
		case 'Windows':
			if ($test[1] == "MSIE") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleIE.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} elseif ($test[1] == "U") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleFF.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} else {
				$headlinks = Array
				(
				   'stylesheet' => "css/style.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			}
			break;
			
		case 'Macintosh':
			if ($testz[0] == " Gecko") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleFFmac.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} elseif ($testz[0] == " Safari") {
				$headlinks = Array
				(
				   'stylesheet' => "css/styleSFmac.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			} else {
				$headlinks = Array
				(
				   'stylesheet' => "css/stylemac.css###text/css",
				   'shortcut icon' => "http://alvinwoon.com/blog/wp-content/themes/stand/favicon.ico",
				);
			}
			break;
}

$body = "onload=\"\"";

?>