<?php
/* $Id: include.php,v 0.0.0.1 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
* www.be-you.org Include Scripts File
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/* Algoritmo per includere tutte le librerie */
$dh=opendir("libraries");
while ($file=readdir($dh)) {
		$filez = "libraries/$file";
		if ($file != "index.php" && !is_dir($filez)) {
		include $filez;
		}
}
closedir($dh);

$SQLStream = BEYOU_SQL::opendb($mdb);

/* Controllo Utente se la zona � admin */
if (isset($_GET[admin])) if ($_POST[admin] != "adesioni" && $_POST[action] != "addX" || $_POST[admin] != "concorso" && $_POST[action] != "addX") $roar = explode('#', BEYOU_AUTH::checka($REQUEST_URI, "Be-You :: Administrators Zone", $retry));
$usernm = $roar[0];
$level = $roar[1];

define("USERNAME", $usernm);
define("LEVEL", $roar[1]);
define("DATE", date("d.m.y"));
define("TIME", date("H:i:s"));
define("IP", $_SERVER["REMOTE_ADDR"]);

/* Include struttura XHTML & CSS */
include 'defines.php';
include 'headers.php';

/* Algoritmo per includere tutte le strutture XHTML & CSS */
$dh=opendir("content");
while ($file=readdir($dh)) {
		$filez = "content/$file";
		if ($file != "index.php" && !is_dir($filez)) include $filez;
}
closedir($dh);

?>