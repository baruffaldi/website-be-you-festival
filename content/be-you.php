<?php
/* $Id: be-you.php,v 0.0.0.3 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Content Scripts
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/* Funzioni per l'output della struttura XHTML/CSS ed il routing per la navigazione*/
class BEYOU_LAYOUT
{
		function route_section($firstkey, $firstvalue)
		{
				global $ids, $admin, $action, $page, $idsc, $id, $idm, $tbpages, $sort;
				
				switch($firstkey) 
				{
					default:
						print " <p class=\"intro\">
										<img src=\"images/logo.jpg\" alt=\"Be-You\" />
								</p>
								<p id=\"linkintro\">
											<a href=\"javascript:apri('?flash');\" title=\"FLASH VERSION\">FLASH</a> :: <a href=\"javascript:apri('?xhtml');\" title=\"XHTML VERSION\">XHTML</a>
								</p>
								<p id=\"linkintrocopy\">
										@2006 <a id=\"linkintrocopya\" href=\"http://www.kimera-lab.com\" title=\"Kimera Team\">Kimera Team</a>
								</p>";
						break;
					
					case 'flash':
						print "<!--
								<object classid='clsid:d27cdb6e-ae6d-11cf-96b8-444553540000' codebase='http://fpdownload.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=8,0,0,0' width='1024' height='768' id='index'>
								<param name='allowScriptAccess' value='sameDomain' />
								<param name='movie' value='index.swf' />
								<param name='quality' value='high' />
								<param name='bgcolor' value='#FFFFFF' />
								<embed src='index.swf' quality='high' bgcolor='#FFFFFF' width='1024' height='768' name='index' align='middle' allowScriptAccess='sameDomain' type='application/x-shockwave-flash' pluginspage='http://www.macromedia.com/go/getflashplayer' />
								</object>--><center><img src=\"images/flash.jpg\" alt=\"Work in progress..\" /></center><br />
								<div class=\"footer\">
								" . BEYOU_LAYOUT::foot() . "
								</div>";
						break;
						
					case 'xhtml':
						print "
						<div class=\"container\">
							<div class=\"navigation\">
								" . BEYOU_LAYOUT::navigation() . BEYOU_LAYOUT::showsponsor() . "
							</div>
							<div class=\"content\">
								<div class=\"header\">
									<div class=\"news\">
									" . BEYOU_LAYOUT::news() . "
									</div>
								</div>
								<div class=\"pages\">
									" . BEYOU_LAYOUT::pages() . "
								</div>
							</div>
							<div class=\"footer\">
								" . BEYOU_LAYOUT::foot() . "
							</div>
						</div>";
						break;
						
					case 'page':
						if (!empty($page)) {
								print "<div class=\"container\">
									<div class=\"navigation\">
										" . BEYOU_LAYOUT::navigation() . "
									</div>
									<div class=\"content\">
										<div class=\"header\">
											<div class=\"news\">
												" . BEYOU_LAYOUT::news() . "
											</div>
										</div>
										<div class=\"pages\">";
										
								if ($page == "gallery") {
										print BEYOU_LAYOUT::showgallery();
								} else {
										print BEYOU_LAYOUT::pageread($page);
								}
								
								$SQLresult = BEYOU_SQL::select($tbpages, '*', "`sid` = '$page'", $limit, "$sort");
								$line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
								
								if ($line[status] == "2") print "\n\n<br />\n<a href=\"javascript:history.back()\" title=\"Torna indietro\">Torna indietro</a>\n";
								print "</div>
									</div>
									<div class=\"footer\">
										" . BEYOU_LAYOUT::foot() . "
									</div>
								</div>";
						}
						break;
						
					case 'login':
						print "Retry login";
						break;
						
					case 'admin':
						echo "
										<div class=\"container\">
											<div class=\"heads\">
												<p id=\"titlez\"></p>
											</div>
											<div class=\"navigation\">";
											
						if ($admin == "adesioni" && $action == "addX" || $admin == "concorso" && $action == "addX" || $admin == "workshop" && $action == "addX" || $action == "modX") {} else {print BEYOU_ADMIN::navigation();}
						
						print "				</div>
											<div class=\"content\">";
											
						switch($admin) {
							default:
								echo "<h2>Amministrazione sito</h2><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
								break;
								
							case 'helpdesk':
								switch($action) {
									default:
										print BEYOU_ADMIN::helpdesk();
										break;
										
									case 'go':
										print BEYOU_ACTIONS::helpdesk();
										break;
								}
								break;
								
							case 'denied':
								print "Questa pagina � protetta a titolo di contenuto statico<br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
								break;
								
							case 'galleria':
								switch($action) {
									default:
										print BEYOU_ADMIN::galleria();
										break;
										
									case 'add':
										print BEYOU_ADMIN::addgalleria();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modgalleria($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delgalleriaX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addgalleriaX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modgalleriaX($idm);
										break;
								}
								break;
								
							case 'sponsor':
								switch($action) {
									default:
										print BEYOU_ADMIN::sponsor();
										break;
										
									case 'add':
										print BEYOU_ADMIN::addsponsor();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modsponsor($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delsponsorX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addsponsorX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modsponsorX($idm);
										break;
								}
								break;
								
							case 'news':
								switch($action) {
									default:
										print BEYOU_ADMIN::news();
										break;
										
									case 'add':
										print BEYOU_ADMIN::addnews();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modnews($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delnewsX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addnewsX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modnewsX($idm);
										break;
								}
								break;
								
							case 'articoli':
								switch($action) {
									default:
										print BEYOU_ADMIN::pages();
										break;
										
									case 'add':
										print BEYOU_ADMIN::addpages();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modpages($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delpagesX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addpagesX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modpagesX($idm);
										break;
								}
								break;
								
							case 'users':
								switch($action) {
									default:
										print BEYOU_ADMIN::users();
										break;
										
									case 'add':
										print BEYOU_ADMIN::addusers();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modusers($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delusersX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addusersX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modusersX($id);
										break;
								}
								break;
								
							case 'adesioni':
								switch($action) {
									default:
										print BEYOU_ADMIN::ades();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modades($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::deladesX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addadesX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modadesX($id);
										break;
								}
								break;
								
							case 'concorso':
								switch($action) {
									default:
										print BEYOU_ADMIN::concorso();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modconcorso($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delconcorsoX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addconcorsoX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modconcorsoX($id);
										break;
								}
								break;
								
							case 'workshop':
								switch($action) {
									default:
										print BEYOU_ADMIN::workshop();
										break;
										
									case 'edit':
										print BEYOU_ADMIN::modworkshop($id);
										break;
										
									case 'del':
										print BEYOU_ACTIONS::delworkshopX($ids);
										break;
										
									case 'addX':
										print BEYOU_ACTIONS::addworkshopX($id);
										break;
										
									case 'modX':
										print BEYOU_ACTIONS::modworkshopX($id);
										break;
								}
								break;
						}
						print "
											</div>
											<div class=\"footer\">
												" . BEYOU_ADMIN::foot() . "
											</div>
										</div>";
						break;
				}
		}

		function navigation() 
		{
				global $tbpages, $sort, $baselink;

				$return = "\t\t<img src=\"images/title.png\" alt=\"14_15 luglio 2006 :: Genova Spezia\" /><br />\n<br />\n\t\t<a href=\"$baselink?xhtml\" title=\"home\">home</a>\n";
		        $SQLresult = BEYOU_SQL::select($tbpages, '*', "`status` = '0'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "\t\t\t<br />\n<a href=\"$baselink?page={$line[sid]}\" title=\"{$line[titolo]}\">{$line[sid]}</a>\n";
				}
				//$return .= "\t\t<br />\n\t\t<a href=\"$baselink?admin\" title=\"admin\">login</a>\n";
				
				return $return;
		}
		
		function news() 
		{
				global $tbnews, $sort;
				$return = "<table>\n";

		        $SQLresult = BEYOU_SQL::select($tbnews, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "
								\t<tr class=\"head\">\n
								\t\t<td>\n<!--
								\t\t\t{$line[data]}<br />\n-->
								\t\t\t" . htmlspecialchars($line[titolo]) . "\n
								\t\t</td>\n
								\t</tr>\n
								\t<tr>\n
								\t\t<td>\n
								\t\t\t{$line[testo]}\n
								\t\t</td>\n
								\t</tr>\n
								";
				}

				$return .= "</table>\n";
				
				return $return;
		}
		
		function pages() 
		{
				global $tbpages, $sort;
				$return = "<table>\n";

		        $SQLresult = BEYOU_SQL::select($tbpages, '*', "`status` = '1'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "
								\t<tr>\n
								\t\t<td id=\"head\">\n<!--
								\t\t\t{$line[data]}<br />\n-->
								\t\t\t" . htmlspecialchars($line[titolo]) . "\n
								\t\t</td>\n
								\t</tr>\n
								\t<tr>\n
								\t\t<td>\n
								\t\t\t" . str_replace("#@#", ",", $line[testo]) . "\n
								\t\t</td>\n
								\t</tr>\n
								";
				}

				$return .= "</table>\n";
				return $return;
		}
		
		function showsponsor() 
		{
				global $tbsponsor, $sort;
				$return = "<div class=\"partner\" style=\"top: 40px;\">\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'partner'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Con il patrocinio di</p>\n\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}

				$return .= "\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'main'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "\n<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Main</p>\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}

				$return .= "\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'sponsor'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "\n<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Con il sostegno di</p>\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}
				
				
				$return .= "\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'istitutional'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "\n<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Con il patrocinio di</p>\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}
				
				$return .= "\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'media'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "\n<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Con il sostegno multimediale di</p>\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}
				
				$return .= "\n";
				$cc = 0;
		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`cat` = 'techno'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					if ($cc == 0) {
						$return .= "\n<br />\n<br />\n<p id=\"partitle\" style=\"color: #62BA49; font-weight: bold;\">Con il sostegno tecnico</p>\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					} else {
						$return .= "\n<br />\n<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />";
					}
					$cc++;
				}
				
				return $return . "</div>";
		}

		function showgallery()
		{
				global $tbgallery, $sort, $pagina, $cat;
				$return = "<div class=\"galleria\">\n<table>\n";
				
				$result = BEYOU_SQL::select($tbgallery, '*', "`Categoria` = '{$_GET[cat]}'", $maxn, "$sort");

				$limit = BEYOU_CORE::paging($result);
				$bho = "0";
		        $SQLresult = BEYOU_SQL::select($tbgallery, '*', "`Categoria` = '{$_GET[cat]}'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					
					$size = getimagesize("uploads/gallery/{$line[sid]}/{$line[nomefile]}");
					$height = $size[1];
					$width = $size[0];
					if ($width > 327)
					{
						$width = 327;
						$percent = ($size[0] / $width);
						$height = ($size[1] / $percent);
					} 
					else if ($height > 150)
					{
						$height = 150;
						$percent = ($size[1] / $height);
						$width = ($size[0] / $percent);
					}
					$scopata = "0";
					if ($line[tipo] == "Orizzontale") {
						if ($figa == "1") {
							$return .= "\n</table><table><tr><td style=\"border: 1px dotted #62BA49; padding: 7px 7px 7px 7px;\"><a href=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" title=\"{$line[titolo]}\" target=\"_blank\">\n<img width=\"$width\" height=\"$height\" id=\"gallery\" src=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />\n</a><br /><strong>{$line[titolo]}</strong><br />\n{$line[testo]}</tr></td>";
							$figa = "0";
						} else {
							$return .= "\n<tr><td style=\"border: 1px dotted #62BA49; padding: 7px 7px 7px 7px;\"><a href=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" title=\"{$line[titolo]}\" target=\"_blank\">\n<img width=\"$width\" height=\"$height\" id=\"gallery\" src=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />\n</a><br /><strong>{$line[titolo]}</strong><br />\n{$line[testo]}</tr></td>";
							$figa = "0";
						}
					} else {
						if ($figa == $scopata) {
							$return .= "</table><table>\n<tr><td style=\"border: 1px dotted #62BA49; padding: 7px 7px 7px 7px;\"><a href=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" title=\"{$line[titolo]}\" target=\"_blank\">\n<img width=\"$width\" height=\"$height\" id=\"gallery\" src=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />\n</a><br /><strong>{$line[titolo]}</strong><br />\n{$line[testo]}</tr></td>";
							$figa = "1";
						} else {
							$return .= "\n<tr><td style=\"border: 1px dotted #62BA49; padding: 7px 7px 7px 7px;\"><a href=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" title=\"{$line[titolo]}\" target=\"_blank\">\n<img width=\"$width\" height=\"$height\" id=\"gallery\" src=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />\n</a><br /><strong>{$line[titolo]}</strong><br />\n{$line[testo]}</tr></td>";
						}
					}
					$bho++;
				}
				
				

				$return .= "</table>\n</div>\n";
				return $return;
		}
		
		function foot() 
		{
				global $time_start;
			    $time_end = getmicrotime();
			    $ktime = $time_end - $time_start;
				
				return "@2006 <a href=\"http://www.kimera-lab.com\" title=\"Kimera Team\">Kimera Team</a>. All rights reserved :: <a href=\"?page=disclaimer\" title=\"Disclaimer\">Disclaimer</a> :: <a href=\"?page=privacy\" title=\"Privacy\">Privacy</a>
						<br />
						<script type='text/javascript' language='JavaScript' SRC='http://codice.shinystat.com/cgi-bin/getcod.cgi?USER=Nicodemusx'></script>
						<noscript>
							<p style=''><A HREF='http://www.shinystat.com' target='_top'>
							<IMG SRC='http://www.shinystat.com/cgi-bin/shinystat.cgi?USER=Nicodemusx' ALT='ShinyStat' BORDER='0'></A></p>
						</noscript>
						";
		}

		function pageread($sid)
		{
				global $tbpages, $sort;
				$return = "<table>\n";

		        $SQLresult = BEYOU_SQL::select($tbpages, '*', "`sid` = '$sid'", $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "
								\t<tr>\n
								\t\t<td id=\"head\">\n<!--
								\t\t\t{$line[data]}<br />\n-->
								\t\t\t" . htmlspecialchars($line[titolo]) . "\n
								\t\t</td>\n
								\t</tr>\n
								\t<tr>\n
								\t\t<td>\n
								\t\t{$line[testo]}\n
								\t\t</td>\n
								\t</tr>\n
								";
				}

				$return .= "</table>\n";
				return $return;
		}
}
?>