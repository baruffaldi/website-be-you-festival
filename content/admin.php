<?php
/* $Id: admin.php,v 0.0.0.3 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Admin Scripts
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/* Funzioni riguardanti la sezione admin */
class BEYOU_ADMIN
{
		function navigation()
		{
				$return = "
							\t\t<a href=\"$baselink?xhtml\" title=\"Be-You\">Be-You</a>\n
							\t\t<br />\n
							\t\t<br />\n
							\t\t<a href=\"$baselink?admin=news\" title=\"news\">News</a><br />\n
							\t\t<a href=\"$baselink?admin=articoli\" title=\"Articoli\">Articoli</a><br /><br />\n
							\t\t<a href=\"$baselink?admin=galleria\" title=\"Galleria\">Galleria</a><br />\n
							\t\t<a href=\"$baselink?admin=sponsor\" title=\"sponsor\">Sponsor</a><br />\n<br />\n
							\t\t<a href=\"$baselink?admin=adesioni\" title=\"Adesioni\">Adesioni</a><br />\n
							\t\t<a href=\"$baselink?admin=concorso\" title=\"Concorso\">Concorso</a><br />\n
							\t\t<a href=\"$baselink?admin=workshop\" title=\"Workshop\">Workshop</a><br /><br />\n
							\t\t<a href=\"$baselink?admin=users\" title=\"Users\">Users</a><br /><br />\n
							\t\t<a href=\"$baselink?admin=helpdesk\" title=\"HelpDesk\">Help</a><br />\n
				";
				
				return $return;
		}
		
		function foot()
		{
				global $time_start;
			    $time_end = getmicrotime();
			    $ktime = $time_end - $time_start;
				
				return "Page rendered in: " . substr($ktime, 0, 6) . "seconds<br />
						@2006 <a href=\"http://www.kimera-lab.com\" title=\"Kimera Team\">Kimera Team</a>";
				//return "@2006 <a href=\"http://www.kimera-lab.com\" alt=\"Kimera Team\">Kimera Team</a>";
		}
		
		function helpdesk()
		{
				global $baselink;
				
				$return = " \t\t<h3>Modulo di segnalazione</h3><br />\n
		 		 		\t\t<br />\n
						\t\t<center><table><tr><td style='text-align: left'>\n
				  	    \t<form method='post' action='$baselink'>\n
		 		 	    \t\t<input type='hidden' name='admin' value='helpdesk' />\n
						\t\t<input type='hidden' name='action' value='go' />\n
						\t\tSubject:</td><td><select id='submitt' name='subject'>\n
						\t\t\t<option value='Malfunzionamenti'>Malfunzionamenti</option>\n
						\t\t\t<option value='Rifiniture'>Rifiniture</option>\n
						\t\t\t<option value='Informazioni'>Informazioni</option>\n
						\t\t</select></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tMessaggio:</td><td><textarea id='txtbox' name='mex' cols='10' rows='10' wrap='off'></textarea></td></tr></table></center><br />\n
		 		 	    \t\t<input type='submit' value='Inserisci!' /><br />\n
		 		 	    \t</form>\n";
					
				return $return;
		}
		
		function news()
		{
				global $tbnews, $sort, $baselink;
				$return = "
				<a href=\"$baselink?admin=news&amp;action=add\" title=\"Aggiungi news\">Aggiungi news</a><br />
				<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='news' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=news&amp;sort=id\">ID</a></td><td><a href=\"?admin=news&amp;sort=user\">Utente</a></td><td><a href=\"?admin=news&amp;sort=data\">Data</a></td><td><a href=\"?admin=news&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=news&amp;sort=titolo\">Titolo</a></td><td><a href=\"?admin=news&amp;sort=testo\">Testo</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbnews, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-uno\"><td><a href=\"javascript:apri('$baselink?admin=news&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[titolo]}\">{$line[id]}</a></td><td>{$line[user]}</td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=news&amp;action=edit&amp;id={$line[id]}');\">"; 
					
					if(strlen($line[titolo]) >= "43") { 
						$return .= htmlspecialchars(substr($line[titolo], 0, 43)) . "</a>..."; 
					} else { 
						$return .= htmlspecialchars($line[titolo]) . "</a>"; 
					} 
					
					$return .= "</a></td><td><a href=\"javascript:apri('$baselink?admin=news&amp;action=edit&amp;id={$line[id]}');\">"; 
					
					if(strlen($line[testo]) >= "43") { 
						$return .= htmlspecialchars(substr($line[testo], 0, 43)) . "</a>..."; 
					} else { 
						$return .= htmlspecialchars($line[testo]) . "</a>"; 
					} 
					
					$return .= "</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
					//$return .= "<tr id=\"row-due\"><td><a href=\"$baselink?admin=news&amp;action=edit&amp;id={$line[id]}\" title=\"{$line[titolo]}\">{$line[id]}</a></td><td>{$line[user]}</td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"$baselink?admin=news&amp;action=edit&amp;id={$line[id]}\">{$line[titolo]}</a></td><td><a href=\"$baselink?admin=news&amp;action=edit&amp;id={$line[id]}\">".substr($line[testo], 0, 60)."</a>...</td><td></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function pages()
		{
				global $tbpages, $sort, $baselink;
				$return = "
				<a href=\"$baselink?admin=articoli&amp;action=add\" title=\"Aggiungi un articolo\">Aggiungi un articolo</a><br />
				<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='articoli' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=articoli&amp;sort=id\">ID</a></td><td><a href=\"?admin=articoli&amp;sort=sid\">SID</a></td><td><a href=\"?admin=articoli&amp;sort=user\">Utente</a></td><td><a href=\"?admin=articoli&amp;sort=data\">Data</a></td><td><a href=\"?admin=articoli&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=articoli&amp;sort=titolo\">Titolo</a></td><td><a href=\"?admin=articoli&amp;sort=testo\">Testo</a></td><td><a href=\"?admin=articoli&amp;sort=status\">Tipo</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbpages, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-uno\"><td><a href=\"javascript:apri('$baselink?admin=articoli&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[titolo]}\">{$line[id]}</a></td><td>{$line[sid]}</td><td>{$line[user]}</td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=articoli&amp;action=edit&amp;id={$line[id]}');\">"; 
					
					if(strlen($line[titolo]) >= "33") { 
						$return .= htmlspecialchars(substr($line[titolo], 0, 33)) . "</a>..."; 
					} else { 
						$return .= htmlspecialchars($line[titolo]) . "</a>"; 
					} 
					
					$return .= "</a></td><td><a href=\"javascript:apri('$baselink?admin=articoli&amp;action=edit&amp;id={$line[id]}');\">"; 
					
					if(strlen($line[testo]) >= "33") { 
						$return .= htmlspecialchars(substr($line[testo], 0, 33)) . "</a>..."; 
					} else { 
						$return .= htmlspecialchars($line[testo]) . "</a>"; 
					} 
					
					$return .= "</td><td>";
					
					if ($line[status] == "0") $line[status] = "Pagina";
					if ($line[status] == "1") $line[status] = "Post";
					if ($line[status] == "2") $line[status] = "Invisibile";
					if ($line[perms] == "1") {
						$return .= "{$line[status]}</td><td></td></tr>\n";
					} else {
						$return .= "{$line[status]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
					}
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function users()
		{
				global $tbusers, $sort, $baselink;
				$return = "
				<a href=\"$baselink?admin=users&amp;action=add\" title=\"Aggiungi un utente\">Aggiungi un utente</a><br />
				<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='users' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=users&amp;sort=id\">ID</a></td><td><a href=\"?admin=users&amp;sort=username\">Username</a></td><td><a href=\"?admin=users&amp;sort=nome\">Nome</a></td><td><a href=\"?admin=users&amp;sort=cognome\">Cognome</a></td><td><a href=\"?admin=users&amp;sort=email\">Email</a></td><td><a href=\"?admin=users&amp;sort=lastlogin\">Ultimo Login</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbusers, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=users&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[nome]} {$line[cognome]}\">{$line[id]}</a></td><td><a href=\"javascript:apri('$baselink?admin=users&amp;action=edit&amp;id={$line[id]}');\">{$line[username]}</a></td><td>{$line[name]}</td><td>{$line[surname]}</td><td><a href=\"mailto:{$line[email]}\" title=\"Mail me\">{$line[email]}</a></td><td>{$line[lastlogin]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function ades()
		{
				global $tbades, $sort, $baselink;
				$return = "Lista di iscrizioni al festival \">Be-You<\" come collaboratore<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='adesioni' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=adesioni&amp;sort=id\">ID</a></td><td><a href=\"?admin=adesioni&amp;sort=data\">Data</a></td><td><a href=\"?admin=adesioni&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=adesioni&amp;sort=cognome\">Cognome</a></td><td><a href=\"?admin=adesioni&amp;sort=nome\">Nome</a></td><td><a href=\"?admin=adesioni&amp;sort=nascita\">Data di nascita</a></td><td><a href=\"?admin=adesioni&amp;sort=sesso\">Sesso</a></td><td><a href=\"?admin=adesioni&amp;sort=posto\">Richiesta</a></td><td><a href=\"?admin=adesioni&amp;sort=status\">Stato</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbades, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=adesioni&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[id]}</a></td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=adesioni&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[cognome]}</a></td><td><a href=\"javascript:apri('$baselink?admin=adesioni&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[nome]}</a></td><td>{$line[nascita]}</td><td>{$line[sesso]}</td><td>{$line[posto]}</td><td>";
					if ($line[status] == "0" || empty($line[status])) $line[status] = "In attesa";
					if ($line[status] == "2") $line[status] = "Approvata";
					if ($line[status] == "1") $line[status] = "Bocciata";
					$return .= "{$line[status]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function concorso()
		{
				global $tbconc, $sort, $baselink;
				$return = "Lista di iscrizioni al concorso \">Be-You< :: Benetton\"<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='concorso' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=concorso&amp;sort=id\">ID</a></td><td><a href=\"?admin=concorso&amp;sort=data\">Data</a></td><td><a href=\"?admin=concorso&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=concorso&amp;sort=cognome\">Cognome</a></td><td><a href=\"?admin=concorso&amp;sort=nome\">Nome</a></td><td><a href=\"?admin=concorso&amp;sort=nascita\">Data di nascita</a></td><td><a href=\"?admin=concorso&amp;sort=residenza\">Residenza</a></td><td><a href=\"?admin=concorso&amp;sort=disciplina\">Disciplina</a></td><td><a href=\"?admin=concorso&amp;sort=status\">Stato</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbconc, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=concorso&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[id]}</a></td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=concorso&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[cognome]}</a></td><td><a href=\"javascript:apri('$baselink?admin=concorso&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[nome]}</a></td><td>{$line[nascita]}</td><td>{$line[residenza]}</td><td>{$line[disciplina]}</td><td>";
					if ($line[status] == "0" || empty($line[status])) $line[status] = "In attesa";
					if ($line[status] == "2") $line[status] = "Approvata";
					if ($line[status] == "1") $line[status] = "Bocciata";
					$return .= "{$line[status]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function workshop()
		{
				global $tbwork, $sort, $baselink;
				$return = "Lista di iscrizioni al workshop \"trainSPOTting\"<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='workshop' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=workshop&amp;sort=id\">ID</a></td><td><a href=\"?admin=workshop&amp;sort=data\">Data</a></td><td><a href=\"?admin=workshop&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=workshop&amp;sort=cognome\">Cognome</a></td><td><a href=\"?admin=workshop&amp;sort=nome\">Nome</a></td><td><a href=\"?admin=workshop&amp;sort=citta\">Citt�</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbwork, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=workshop&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[id]}</a></td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=workshop&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[cognome]}</a></td><td><a href=\"javascript:apri('$baselink?admin=workshop&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[cognome]} {$line[nome]}\">{$line[nome]}</a></td><td>{$line[citta]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function galleria()
		{
				global $tbgallery, $sort, $baselink;
				$return = "
				<a href=\"$baselink?admin=galleria&amp;action=add\" title=\"Aggiungi un utente\">Aggiungi un immagine</a><br />
				<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='galleria' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=galleria&amp;sort=id\">ID</a></td><td><a href=\"?admin=galleria&amp;sort=user\">Username</a></td><td><a href=\"?admin=workshop&amp;sort=data\">Data</a></td><td><a href=\"?admin=workshop&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=galleria&amp;sort=nomefile\">Nome File</a></td><td><a href=\"?admin=galleria&amp;sort=titolo\">Titolo</a></td><td><a href=\"?admin=galleria&amp;sort=testo\">Descrizione</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbgallery, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=galleria&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[filename]} {$line[titolo]}\">{$line[id]}</a></td><td>{$line[user]}</td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=galleria&amp;action=edit&amp;id={$line[id]}');\">{$line[nomefile]}</a></td><td><a href=\"javascript:apri('$baselink?admin=galleria&amp;action=edit&amp;id={$line[id]}');\">{$line[titolo]}</a></td><td>{$line[testo]}</td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function sponsor()
		{
				global $tbsponsor, $sort, $baselink;
				$return = "
				<a href=\"$baselink?admin=sponsor&amp;action=add\" title=\"Aggiungi un utente\">Aggiungi uno sponsor</a><br />
				<br />
				<form method='post' action='$baselink'>\n
				<input type='hidden' name='admin' value='sponsor' />\n
				<input type='hidden' name='action' value='del' />\n
				<table><tr id=\"tabletitles\"><td><a href=\"?admin=sponsor&amp;sort=id\">ID</a></td><td><a href=\"?admin=sponsor&amp;sort=user\">Username</a></td><td><a href=\"?admin=sponsor&amp;sort=data\">Data</a></td><td><a href=\"?admin=sponsor&amp;sort=ora\">Ora</a></td><td><a href=\"?admin=sponsor&amp;sort=nomefile\">Nome File</a></td><td><a href=\"?admin=sponsor&amp;sort=cat\">Categoria</a></td><td><a href=\"?admin=sponsor&amp;sort=titolo\">Titolo</a></td><td>Elimina</td></tr>\n";

		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', $where, $limit, "$sort");
		        while($line = mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
					$return .= "<tr id=\"row-due\"><td><a href=\"javascript:apri('$baselink?admin=sponsor&amp;action=edit&amp;id={$line[id]}');\" title=\"{$line[filename]} {$line[titolo]}\">{$line[id]}</a></td><td>{$line[user]}</td><td>{$line[data]}</td><td>{$line[ora]}</td><td><a href=\"javascript:apri('$baselink?admin=sponsor&amp;action=edit&amp;id={$line[id]}');\">{$line[nomefile]}</a></td><td>{$line[cat]}</td><td><a href=\"javascript:apri('$baselink?admin=sponsor&amp;action=edit&amp;id={$line[id]}');\">{$line[titolo]}</a></td><td><input type='checkbox' name='".$line[id]."' value='1' /></td></tr>\n";
				}
				
				$return .= "</table><br /><input id=\"submitt\" type='submit' value='Elimina!' /><br /><br /><br /><br /><br />\n
						   			   </form>\n";
				return $return;
		}
		
		function addpages()
		{
		        global $baselink;

				$sid = md5(rand(0, 99999999));
				 
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='articoli' />\n
						\t\t<input type='hidden' name='action' value='addX' />\n
						\t\t<input type='hidden' name='user' value='".USERNAME."' />\n
						\t\t<input type='hidden' name='sid' value='$sid' />\n
						\t\t<input type='hidden' name='data' value='".DATE."' />\n
						\t\t<input type='hidden' name='ora' value='".TIME."' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
						\t\tTipo di articolo:</td><td><select id=\"submitt\" name=\"status\"><option value='1'/>Post</option><option value='0'/>Pagina</option><option value='2'/>Invisibile</option></select></td></tr><tr><td style='text-align: left'>\n
						\t\tNome(se pagina):</td><td><input id=\"submitt\" type='text' name=\"sid\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>
<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\"></textarea></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Inserisci!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function addnews()
		{
		        global $baselink;

				$sid = md5(rand(0, 99999999));
				 
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='news' />\n
						\t\t<input type='hidden' name='action' value='addX' />\n
						\t\t<input type='hidden' name='user' value='".USERNAME."' />\n
						\t\t<input type='hidden' name='sid' value='$sid' />\n
						\t\t<input type='hidden' name='data' value='".DATE."' />\n
						\t\t<input type='hidden' name='ora' value='".TIME."' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>
<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\"></textarea></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Inserisci!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function addusers()
		{
		        global $baselink;
				 
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='users' />\n
						\t\t<input type='hidden' name='action' value='addX' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
		 		 	    \t\tUsername:</td><td><input id=\"submitt\" type='text' name=\"username\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tPassword:</td><td><input id=\"submitt\" type='password' name=\"passwd\" /><br /></td></tr><tr><td style='text-align: left'>\n
						\t\tNome:</td><td><input id=\"submitt\" type='text' name=\"name\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tCognome:</td><td><input id=\"submitt\" type='text' name=\"surname\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\teMail:</td><td><input id=\"submitt\" type='text' name=\"email\" /></td></tr></table>\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Inserisci!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function addgalleria()
		{
		        global $baselink;
				
				$sid = md5(rand(0, 99999999));

		 		print "\t<form method='post' action='$baselink' ENCTYPE=\"multipart/form-data\">\n
						\t\t<input type='hidden' name='admin' value='galleria' />\n
						\t\t<input type='hidden' name='action' value='addX' />\n
						\t\t<input type='hidden' name='sid' value='$sid' />\n
						\t\t<input type='hidden' name='user' value='".USERNAME."' />\n
						\t\t<input type='hidden' name='data' value='".DATE."' />\n
						\t\t<input type='hidden' name='ora' value='".TIME."' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
						\t\tImmagine:</td><td><input name='file' type='file' /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>
<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\"></textarea></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Inserisci!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function addsponsor()
		{
		        global $baselink;
				
				$sid = md5(rand(0, 99999999));

		 		print "\t<form method='post' action='$baselink' ENCTYPE=\"multipart/form-data\">\n
						\t\t<input type='hidden' name='admin' value='sponsor' />\n
						\t\t<input type='hidden' name='action' value='addX' />\n
						\t\t<input type='hidden' name='sid' value='$sid' />\n
						\t\t<input type='hidden' name='user' value='".USERNAME."' />\n
						\t\t<input type='hidden' name='data' value='".DATE."' />\n
						\t\t<input type='hidden' name='ora' value='".TIME."' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
						\t\tTipo di sponsor:</td><td><select id=\"submitt\" name=\"cat\"><option value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option></select></td></tr><tr><td style='text-align: left'>\n
						\t\tImmagine:</td><td><input name='file' type='file' /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" /></td></tr></table>\n<br />
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Inserisci!\" /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modpages($id)
		{
		        global $baselink, $sort, $tbpages;

		        $SQLresult = BEYOU_SQL::select($tbpages, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
				
		 		print " \t<form method='post' action='$baselink'>\n";
						
				if ($line[perms] == "1") {
						print "\t\t<input type='hidden' name='admin' value='denied' />\n";
				} else {
						print "\t\t<input type='hidden' name='admin' value='articoli' />\n";
				}
				print " \t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
						\t\tData:</td><td><input id=\"submitt\" type='text' name=\"data\" value=\"{$line[data]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td><input id=\"submitt\" type='text' name=\"ora\" value=\"{$line[ora]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tTipo di articolo:</td><td><select id=\"submitt\" name=\"status\">";
						
				if ($line[status] == "0") {
						print "<option value='1'/>Post</option><option selected=\"selected\" value='0'/>Pagina</option><option value='2'/>Invisibile</option>";
				} elseif ($line[status] == "1") {
						print "<option selected=\"selected\" value='1'/>Post</option><option value='0'/>Pagina</option><option value='2'/>Invisibile</option>";
				} else {
						print "<option value='1'/>Post</option><option value='0'/>Pagina</option><option selected=\"selected\" value='2'/>Invisibile</option>";
				}
						
		 		print " </select></td></tr><tr><td style='text-align: left'>\n";
				if ($line[status] == "0") print "\t\tNome(se pagina):</td><td><input id=\"submitt\" type='text' name=\"sid\"  value=\"{$line[sid]}\" /></td></tr><tr><td style='text-align: left'>\n";
				print " \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" value=\"{$line[titolo]}\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>";
						
				if ($line[perms] == "1") {
						print BEYOU_CORE::formattext(str_replace("#@#", ",", $line[testo])) . "</td></tr><tr><td style='text-align: left'>\n";
				} else {
						print "<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\">" . BEYOU_CORE::formattext(str_replace("#@#", ",", $line[testo])) . "</textarea></td></tr><tr><td style='text-align: left'>\n";
				}
				print " \t\tID:</td><td><input id=\"submitt\" type='text' name=\"idm\" value=\"{$line[id]}\" /></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modnews($id)
		{
		        global $baselink, $tbnews, $sort;

		        $SQLresult = BEYOU_SQL::select($tbnews, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
				
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='news' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left'>\n
						\t\tData:</td><td><input id=\"submitt\" type='text' name=\"data\" value=\"{$line[data]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td><input id=\"submitt\" type='text' name=\"ora\" value=\"{$line[ora]}\" /><br /><br /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" value=\"{$line[titolo]}\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>
<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\">" . BEYOU_CORE::formattext(str_replace("#@#", ",", $line[testo])) . "</textarea></td></tr><tr><td style='text-align: left'>\n
						\t\tID:</td><td><input id=\"submitt\" type='text' name=\"idm\" value=\"{$line[id]}\" /></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modusers($id)
		{
		        global $baselink, $tbusers, $sort;


		        $SQLresult = BEYOU_SQL::select($tbusers, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
				
		 		print " \tLasciare il campo \"Password\" bianco se non la si vuole modificare<br />
						\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='users' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<br />\n
						\t\t<table id=\"modifica\"><tr><td>\n
						\t\tUltimo Login:</td><td>{$line[lastlogin]}</td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tUsername:</td><td><input id=\"submitt\" type='text' name=\"username\" value=\"{$line[username]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tPassword:</td><td><input id=\"submitt\" type='password' name=\"passwd\" value=\"\" /><br /></td></tr><tr><td style='text-align: left'>\n
						\t\tNome:</td><td><input id=\"submitt\" type='text' name=\"name\" value=\"{$line[name]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tCognome:</td><td><input id=\"submitt\" type='text' name=\"surname\" value=\"{$line[surname]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\teMail:</td><td><input id=\"submitt\" type='text' name=\"email\" value=\"{$line[email]}\" /></td></tr></table>\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modades($id)
		{
		        global $baselink, $tbades, $sort;


		        $SQLresult = BEYOU_SQL::select($tbades, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);

		 		print " \t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='adesioni' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<table id=\"modifica\">
						<tr><td style='text-align: left'>Data:</td><td>{$line[data]}</td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td>{$line[ora]}</td></tr><tr><td style='text-align: left'>\n
						\t\tIP:</td><td>{$line[ip]}</td></tr><tr><td style='text-align: left'>\n
						\t\tCognome:</td><td><input id=\"submitt\" type='text' name=\"cognome\" value=\"{$line[cognome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tNome:</td><td><input id=\"submitt\" type='text' name=\"nome\" value=\"{$line[nome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tData di nascita:</td><td><input id=\"submitt\" type='text' name=\"nascita\" value=\"{$line[nascita]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tSesso:</td><td><input id=\"submitt\" type='text' name=\"sesso\" value=\"{$line[sesso]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tAbitazione:</td><td><input id=\"submitt\" type='text' name=\"abitazione\" value=\"{$line[abitazione]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tOccupazione:</td><td><input id=\"submitt\" type='text' name=\"occupazione\" value=\"{$line[occupazione]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tTelefono:</td><td><input id=\"submitt\" type='text' name=\"tel\" value=\"{$line[tel]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\teMail:</td><td><input id=\"submitt\" type='text' name=\"email\" value=\"{$line[email]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tFissa:</td><td><input id=\"submitt\" type='text' name=\"fissa\" value=\"{$line[fissa]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tNon rinuncerebbe mai a:</td><td><input id=\"submitt\" type='text' name=\"norinuncia\" value=\"{$line[norinuncia]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tMiti:</td><td><input id=\"submitt\" type='text' name=\"miti\" value=\"{$line[miti]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tGiornata tipo nel 2016:</td><td><input id=\"submitt\" type='text' name=\"miti\" value=\"{$line[giornatatipo]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tEsperienze precedenti:</td><td><input id=\"submitt\" type='text' name=\"esperienze\" value=\"{$line[esperienze]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tRichiesta posto:</td><td><input id=\"submitt\" type='text' name=\"posto\" value=\"{$line[posto]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tMezzo:</td><td><input id=\"submitt\" type='text' name=\"motorizzato\" value=\"{$line[motorizzato]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tNotes:</td><td><input id=\"submitt\" type='text' name=\"note\" value=\"{$line[note]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tStatus:</td><td><select id=\"submitt\" name=\"status\">";
						
				if ($line[status] == "1") {
						print "<option value='2'/>Approvato</option><option selected=\"selected\" value='1'/>Rifiutato</option><option value='0'/>In attesa</option>";
				} elseif ($line[status] == "2") {
						print "<option selected=\"selected\" value='2'/>Approvato</option><option value='1'/>Rifiutato</option><option value='0'/>In attesa</option>";
				} else {
						print "<option value='2'/>Approvato</option><option value='1'/>Rifiutato</option><option selected=\"selected\" value='0'/>In attesa</option>";
				}
						
		 		print " </select><td></td></tr></table>\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modconcorso($id)
		{
		        global $baselink, $tbconc, $sort;


		        $SQLresult = BEYOU_SQL::select($tbconc, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);

		 		print " \t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='concorso' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<table id=\"modifica\">
						<tr><td style='text-align: left'>Data:</td><td>{$line[data]}</td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td>{$line[ora]}</td></tr><tr><td style='text-align: left'>\n
						\t\tIP:</td><td>{$line[ip]}</td></tr><tr><td style='text-align: left'>\n
						\t\tCognome:</td><td><input id=\"submitt\" type='text' name=\"cognome\" value=\"{$line[cognome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tNome:</td><td><input id=\"submitt\" type='text' name=\"nome\" value=\"{$line[nome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tData di nascita:</td><td><input id=\"submitt\" type='text' name=\"nascita\" value=\"{$line[nascita]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tResidenza:</td><td><input id=\"submitt\" type='text' name=\"residenza\" value=\"{$line[residenza]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\teMail:</td><td><input id=\"submitt\" type='text' name=\"email\" value=\"{$line[email]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tGSM:</td><td><input id=\"submitt\" type='text' name=\"gsm\" value=\"{$line[gsm]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tTelefono:</td><td><input id=\"submitt\" type='text' name=\"tel\" value=\"{$line[tel]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tDisciplina:</td><td><input id=\"submitt\" type='text' name=\"disciplina\" value=\"{$line[disciplina]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tAltro:</td><td><input id=\"submitt\" type='text' name=\"altro\" value=\"{$line[altro]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tEsperienze Precedenti:</td><td><input id=\"submitt\" type='text' name=\"esperienze\" value=\"{$line[esperienze]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tNotes:</td><td><input id=\"submitt\" type='text' name=\"note\" value=\"{$line[note]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tStatus:</td><td><select id=\"submitt\" name=\"status\">";
						
				if ($line[status] == "1") {
						print "<option value='2'/>Approvato</option><option selected=\"selected\" value='1'/>Rifiutato</option><option value='0'/>In attesa</option>";
				} elseif ($line[status] == "2") {
						print "<option selected=\"selected\" value='2'/>Approvato</option><option value='1'/>Rifiutato</option><option value='0'/>In attesa</option>";
				} else {
						print "<option value='2'/>Approvato</option><option value='1'/>Rifiutato</option><option selected=\"selected\" value='0'/>In attesa</option>";
				}
						
		 		print " </select><td></td></tr></table>\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modworkshop($id)
		{
		        global $baselink, $tbwork, $sort;


		        $SQLresult = BEYOU_SQL::select($tbwork, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);

		 		print " \t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='workshop' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<table id=\"modifica\">
						<tr><td style='text-align: left'>Data:</td><td>{$line[data]}</td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td>{$line[ora]}</td></tr><tr><td style='text-align: left'>\n
						\t\tIP:</td><td>{$line[ip]}</td></tr><tr><td style='text-align: left'>\n
						\t\tCognome:</td><td><input id=\"submitt\" type='text' name=\"cognome\" value=\"{$line[cognome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tNome:</td><td><input id=\"submitt\" type='text' name=\"nome\" value=\"{$line[nome]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tCitt�:</td><td><input id=\"submitt\" type='text' name=\"nascita\" value=\"{$line[citta]}\" /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tNotes:</td><td><input id=\"submitt\" type='text' name=\"note\" value=\"{$line[note]}\" /></td></tr></table>\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}

		
		function modgalleria($id)
		{
		        global $baselink, $tbgallery, $sort;

		        $SQLresult = BEYOU_SQL::select($tbgallery, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
				
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='galleria' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left' colspan='2'>\n";
					$size = getimagesize("uploads/gallery/{$line[sid]}/{$line[nomefile]}");
					$height = $size[1];
					$width = $size[0];
					if ($height > 150)
					{
						$height = 150;
						$percent = ($size[1] / $height);
						$width = ($size[0] / $percent);
					}
					else if ($width > 150)
					{
						$width = 150;
						$percent = ($size[0] / $width);
						$height = ($size[1] / $percent);
					}
				print "\n<a href=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" title=\"{$line[titolo]}\" target=\"_blank\">\n<img width=\"$width\" height=\"$height\" id=\"gallery\" src=\"uploads/gallery/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\" />\n</a></td></tr><tr><td style='text-align: left'>\n
						\t\tData:</td><td><input id=\"submitt\" type='text' name=\"data\" value=\"{$line[data]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td><input id=\"submitt\" type='text' name=\"ora\" value=\"{$line[ora]}\" /><br /><br /></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" value=\"{$line[titolo]}\" /></td></tr><tr><td style='text-align: left' colspan=2>\n

		<div id=\"quicktags\">
			<script src=\"quicktags.js\" type=\"text/javascript\"></script>
			<script type=\"text/javascript\">if ( typeof tinyMCE == \"undefined\" || tinyMCE.configs.length < 1 ) edToolbar();</script>
		</div></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTesto:</td><td>
<textarea title=\"true\" rows=\"10\" cols=\"40\" name=\"testo\" tabindex=\"2\" id=\"content\">" . BEYOU_CORE::formattext(str_replace("#@#", ",", $line[testo])) . "</textarea></td></tr><tr><td style='text-align: left'>\n
						\t\tID:</td><td><input id=\"submitt\" type='text' name=\"idm\" value=\"{$line[id]}\" /></td></tr></table>

<script type=\"text/javascript\">
<!--
edCanvas = document.getElementById('content');
// This code is meant to allow tabbing from Title to Post (TinyMCE).
if ( tinyMCE.isMSIE )
	document.getElementById('title').onkeydown = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
else
	document.getElementById('title').onkeypress = function (e)
		{
			e = e ? e : window.event;
			if (e.keyCode == 9 && !e.shiftKey && !e.controlKey && !e.altKey) {
				var i = tinyMCE.selectedInstance;
				if(typeof i ==  'undefined')
					return true;
                                tinyMCE.execCommand(\"mceStartTyping\");
				this.blur();
				i.contentWindow.focus();
				e.returnValue = false;
				return false;
			}
		}
//-->
</script><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
		
		function modsponsor($id)
		{
		        global $baselink, $tbsponsor, $sort;

		        $SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`id` = '$id'", $limit, "$sort");
		        $line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
				
		 		print "\t<form method='post' action='$baselink'>\n
						\t\t<input type='hidden' name='admin' value='sponsor' />\n
						\t\t<input type='hidden' name='action' value='modX' />\n
						\t\t<input type='hidden' name='id' value='$id' />\n
						\t\t<br />\n
						\t\t<table><tr><td style='text-align: left' colspan='2'>\n
						\t\t<img src=\"uploads/sponsor/{$line[sid]}/{$line[nomefile]}\" alt=\"{$line[titolo]}\"></td></tr><tr><td style='text-align: left'>\n
						\t\tData:</td><td><input id=\"submitt\" type='text' name=\"data\" value=\"{$line[data]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tOra:</td><td><input id=\"submitt\" type='text' name=\"ora\" value=\"{$line[ora]}\" /><br /><br /></td></tr><tr><td style='text-align: left'>\n
						\t\tTipo di sponsor:</td><td><select id=\"submitt\" name=\"cat\">";
						switch($line[cat]) {
								case 'main':
									print "<option value='partner'/>Partner</option><option selected=\"selected\" value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option>";
									break;
								case 'sponsor':
									print "<option value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option selected=\"selected\" value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option>";
									break;
								case 'partner':
									print "<option selected=\"selected\" value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option>";
									break;
								case 'media':
									print "<option value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option selected=\"selected\" value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option>";
									break;
								case 'techno':
									print "<option value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option selected=\"selected\" value='techno'/>Sponsor Tecnico</option>";
									break;
								case 'istitutional':
									print "<option value='partner'/>Partner</option><option value='main'/>Main Sponsor</option><option value='sponsor'/>Sponsor</option><option selected=\"selected\" value='istitutional'/>Sponsor Istituzionale</option><option value='media'/>Media Partner</option><option value='techno'/>Sponsor Tecnico</option>";
									break;
							}
						print "</select></td></tr><tr><td style='text-align: left'>\n
		 		 	    \t\tTitolo:</td><td><input id=\"submitt\" type='text' name=\"titolo\" value=\"{$line[titolo]}\" /></td></tr><tr><td style='text-align: left'>\n
						\t\tID:</td><td><input id=\"submitt\" type='text' name=\"idm\" value=\"{$line[id]}\" /></td></tr></table><br />\n
		 		 	    \t\t<input id=\"submitt\" type=\"submit\" value=\"Modifica!\" /><br />\n
		 		 	    \t</form>\n";
		}
}
?>