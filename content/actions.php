<?php
/* $Id: actions.php,v 0.0.0.3 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Actions Scripts
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/* Funzioni per la manipolazione del database */
class BEYOU_ACTIONS
{

		function reglogin()
		{
				global $tbusers;
				
				mysql_query("UPDATE $tbusers SET `lastlogin` = '". DATE . "-" . TIME . "' WHERE (`username` = '" . USERNAME . "')");
		}

		function helpdesk()
		{
		        global $tbnews, $baselink;
		        
		 		if (!empty($_POST[message])) {
		 		 	$message = "\t\t\t\tC.C.S. Support system\n\tDate: " . DATE . "\n\tTime: " . TIME . "\n\n\tUser: " . USERNAME . "\n\tSubject: $_POST[subject]\n\n$_POST[message]";
		 		 	mail("filippo@kimera-lab.com","Be-You :: Richiesta Assistenza per {$_POST[subject]}", $_POST[message]);
					mail("patrizio@kimera-lab.com","Be-You :: Richiesta Assistenza per {$_POST[subject]}", $_POST[message]);
		 		}
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=helpdesk\" title=\"Aggiungi un'altra news\">Manda un altro messaggio</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		}

		function addnewsX($id)
		{
		        global $tbnews, $baselink;
		        
				BEYOU_SQL::insertrow($tbnews, "`sid`,`user`,`data`,`ora`,`titolo`,`testo`", '"'.$_POST[sid].'","'.$_POST[user].'","'.$_POST[data].'","'.$_POST[ora].'","'.$_POST[titolo].'","'.BEYOU_CORE::formattextrev($_POST[testo]).'"');
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=news&amp;\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=news&amp;action=add\" title=\"Aggiungi un'altra news\">Aggiungi un'altra news</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		}

		function addpagesX($id)
		{
		        global $tbpages, $baselink;
		        
				BEYOU_SQL::insertrow($tbpages, "`sid`,`user`,`data`,`ora`,`titolo`,`testo`,`status`", '"'.$_POST[sid].'","'.$_POST[user].'","'.$_POST[data].'","'.$_POST[ora].'","'.$_POST[titolo].'","'.BEYOU_CORE::formattextrev($_POST[testo]).'","'.$_POST[status].'"');
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=articoli&amp;\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=articoli&amp;action=add\" title=\"Aggiungi un'altra pagina\">Aggiungi un'altra pagina</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		}

		function addusersX($id)
		{
		        global $tbusers, $baselink;
		        
				BEYOU_SQL::insertrow($tbusers, "`username`,`passwd`,`name`,`surname`,`email`", '"'.$_POST[username].'","'.md5($_POST[passwd]).'","'.$_POST[name].'","'.$_POST[surname].'","'.$_POST[email].'"');
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=users&amp;\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=users&amp;action=add\" title=\"Aggiungi un altro utente\">Aggiungi un altro utente</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		}

		function addadesX($id)
		{
		        global $tbades, $baselink;
				
				if ($_POST[verify] == "on") {
						BEYOU_SQL::insertrow($tbades, "`data`,`ora`,`ip`,`cognome`,`nome`,`nascita`,`sesso`,`abitazione`,`occupazione`,`tel`,`email`,`fissa`,`norinuncia`,`miti`,`giornatatipo`,`esperienze`,`posto`,`motorizzato`", '"'.DATE.'","'.TIME.'","'.IP.'","'.$_POST[cognome].'","'.$_POST[nome].'","'.$_POST[nascita].'","'.$_POST[sesso].'","'.$_POST[abitazione].'","'.$_POST[occupazione].'","'.$_POST[tel].'","'.$_POST[email].'","'.$_POST[fissa].'","'.$_POST[norinuncia].'","'.$_POST[miti].'","'.$_POST[giornatatipo].'","'.$_POST[esperienze].'","'.$_POST[posto].'","'.$_POST[motorizzato].'"');
						print "La tua richiesta di partecipazione al festival come collaboratore � stata registrata correttamente.<br /> Se hai inserito correttamente tutti i campi verrai ricontattato non appena possibile.<br /> Il team di Be You ti ringrazia :)<br />";
						print "<a href=\"$baselink?xhtml\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				} else {
						print "Per la richiesta di collaborazione serve l'approvazione dei Termini di servizio e della Norma sulla privacy.<br />";
						print "<a href=\"$baselink?page=we want you\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				}
		}

		function addconcorsoX($id)
		{
		        global $tbconc, $baselink;
				
				if ($_POST[verify] == "on") {
						BEYOU_SQL::insertrow($tbconc, "`data`,`ora`,`ip`,`cognome`,`nome`,`nascita`,`residenza`,`esperienze`,`email`,`gsm`,`tel`,`disciplina`,`altro`", '"'.DATE.'","'.TIME.'","'.IP.'","'.$_POST[cognome].'","'.$_POST[nome].'","'.$_POST[nascita].'","'.$_POST[residenza].'","'.$_POST[esperienze].'","'.$_POST[email].'","'.$_POST[gsm].'","'.$_POST[tel].'","'.$_POST[disciplina].'","'.$_POST[altro].'"');
						print "La tua richiesta di partecipazione al concorso >Be-You< :: Benetton � stata registrata correttamente.<br /> Se hai inserito correttamente tutti i campi verrai ricontattato non appena possibile.<br /> Il team di Be You ti ringrazia :)<br />";
						print "<a href=\"$baselink?xhtml\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				} else {
						print "Per la richiesta di collaborazione serve l'approvazione dei Termini di servizio e della Norma sulla privacy.<br />";
						print "<a href=\"$baselink?page=concorso\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				}
		}

		function addworkshopX($id)
		{
		        global $tbwork, $baselink;
				
				if ($_POST[verify] == "on") {
						BEYOU_SQL::insertrow($tbwork, "`data`,`ora`,`ip`,`cognome`,`nome`,`citta`", '"'.DATE.'","'.TIME.'","'.IP.'","'.$_POST[cognome].'","'.$_POST[nome].'","'.$_POST[citta].'"');
						print "La tua richiesta di partecipazione al workshop trainSPOTting � stata registrata correttamente.<br /> Il team di Be You ti ringrazia :)<br />";
						print "<a href=\"$baselink?xhtml\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				} else {
						print "Per la richiesta di collaborazione serve l'approvazione dei Termini di servizio e della Norma sulla privacy.<br />";
						print "<a href=\"$baselink?page=workshop\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
				}
		}

		function addgalleriaX($id)
		{
		        global $tbgallery, $baselink, $uploadfile, $tmpsize, $tmpext, $file;

		   		 if (!BEYOU_CORE::is_image($uploadfile)) {
			   		 	BEYOU_CORE::rm("uploads/gallery/".$_POST[sid]);
			   		 	echo "PLEASE, UPLOAD A VALID IMAGE.<br />\n";
		   		 } else {
						BEYOU_SQL::insertrow($tbgallery, "`sid`,`user`,`data`,`ora`,`nomefile`,`titolo`,`testo`", '"'.$_POST[sid].'","'.$_POST[user].'","'.$_POST[data].'","'.$_POST[ora].'","'.$_FILES['file']['name'].'","'.$_POST[titolo].'","'.BEYOU_CORE::formattextrev($_POST[testo]).'"');
				}
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=galleria&amp;\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=galleria&amp;action=add\" title=\"Aggiungi un altra immagine\">Aggiungi un altra immagine</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";
		}

		function addsponsorX($id)
		{
		        global $tbsponsor, $baselink, $uploadfile, $tmpsize, $tmpext, $file;

		   		 if (!BEYOU_CORE::is_image($uploadfile)) {
			   		 	BEYOU_CORE::rm("uploads/gallery/".$_POST[sid]);
			   		 	echo "PLEASE, UPLOAD A VALID IMAGE.<br />\n";
		   		 } else {
						BEYOU_SQL::insertrow($tbsponsor, "`cat`,`sid`,`user`,`data`,`ora`,`nomefile`,`titolo`", '"'.$_POST[cat].'","'.$_POST[sid].'","'.$_POST[user].'","'.$_POST[data].'","'.$_POST[ora].'","'.$_FILES['file']['name'].'","'.$_POST[titolo].'"');
				}
		        
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=sponsor&amp;\" title=\"Torna indietro\">Torna indietro</a> :: <a href=\"$baselink?admin=sponsor&amp;action=add\" title=\"Aggiungi un altro sponsor\">Aggiungi un altro sponsor</a><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br /><br />";		}
		
		function delnewsX($ids)
		{
		        global $tbnews, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbnews, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=news&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delpagesX($ids)
		{
		        global $tbpages, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbpages, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=articoli&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delusersX($ids)
		{
		        global $tbusers, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbusers, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=users&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function deladesX($ids)
		{
		        global $tbades, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbades, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=adesioni&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delconcorsoX($ids)
		{
		        global $tbconc, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbconc, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=concorso&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delworkshopX($ids)
		{
		        global $tbwork, $baselink;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					BEYOU_SQL::deleterow($tbwork, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=workshop&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delgalleriaX($ids)
		{
		        global $tbgallery, $baselink, $sort;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					$SQLresult = BEYOU_SQL::select($tbgallery, '*', "`id` = '$id'", $limit, "$sort");
					$line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
					BEYOU_CORE::rm("uploads/gallery/".$line[sid]);
					BEYOU_SQL::deleterow($tbgallery, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=galleria&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function delsponsorX($ids)
		{
		        global $tbsponsor, $baselink, $sort;

				$ids_arr = explode(",", $ids);
				foreach ($ids_arr as $id) {
					$SQLresult = BEYOU_SQL::select($tbsponsor, '*', "`id` = '$id'", $limit, "$sort");
					$line = mysql_fetch_array($SQLresult, MYSQL_ASSOC);
					BEYOU_CORE::rm("uploads/sponsor/".$line[sid]);
					BEYOU_SQL::deleterow($tbsponsor, "`id` = '$id'");
				}

		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=sponsor&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modnewsX($id)
		{
				global $tbnews, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) { 
							if ($tmp[0] == "testo") {
								mysql_query("UPDATE $tbnews SET ". $tmp[0] . " = '". BEYOU_CORE::formattextrev($tmp[1]) . "' WHERE (`id` = '$id')");
							} elseif ($tmp[0] == "idm") {
								mysql_query("UPDATE $tbnews SET id = '". $tmp[1] . "' WHERE (`id` = '$id')");
							} else {
								mysql_query("UPDATE $tbnews SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
							}
					}
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=news&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modpagesX($id)
		{
				global $tbpages, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) { 
							if ($tmp[0] == "testo") {
								mysql_query("UPDATE $tbpages SET ". $tmp[0] . " = '". BEYOU_CORE::formattextrev($tmp[1]) . "' WHERE (`id` = '$id')");
							} elseif ($tmp[0] == "idm") {
								mysql_query("UPDATE $tbpages SET id = '". $tmp[1] . "' WHERE (`id` = '$id')");
							} else {
								mysql_query("UPDATE $tbpages SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
							}
					}
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=articoli&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modusersX($id)
		{
				global $tbusers, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) {
							if ($tmp[0] == "passwd" && !empty($tmp[1])) mysql_query("UPDATE $tbusers SET ". $tmp[0] . " = '". md5($tmp[1]) . "' WHERE (`id` = '$id')");
					}
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=users&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modadesX($id)
		{
				global $tbades, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) mysql_query("UPDATE $tbades SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=adesioni&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modconcorsoX($id)
		{
				global $tbconc, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) mysql_query("UPDATE $tbconc SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=concorso&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modworkshopX($id)
		{
				global $tbwork, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
					if (!empty($idf)) mysql_query("UPDATE $tbwork SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=workshop&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modgalleriaX($id)
		{
				global $tbgallery, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
							if ($tmp[0] == "testo") {
								mysql_query("UPDATE $tbgallery SET ". $tmp[0] . " = '". BEYOU_CORE::formattextrev($tmp[1]) . "' WHERE (`id` = '$id')");
							} elseif ($tmp[0] == "idm") {
								mysql_query("UPDATE $tbgallery SET id = '". $tmp[1] . "' WHERE (`id` = '$id')");
							} else {
								mysql_query("UPDATE $tbgallery SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
							}
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=galleria&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}

		function modsponsorX($id)
		{
				global $tbsponsor, $baselink, $idsc;
				
				$ids_arr = explode("���", $idsc);
				
				foreach($ids_arr as $idf) {
					$tmp = explode("#", $idf);
							if ($tmp[0] == "testo") {
								mysql_query("UPDATE $tbsponsor SET ". $tmp[0] . " = '". BEYOU_CORE::formattextrev($tmp[1]) . "' WHERE (`id` = '$id')");
							} elseif ($tmp[0] == "idm") {
								mysql_query("UPDATE $tbsponsor SET id = '". $tmp[1] . "' WHERE (`id` = '$id')");
							} else {
								mysql_query("UPDATE $tbsponsor SET ". $tmp[0] . " = '". $tmp[1] . "' WHERE (`id` = '$id')");
							}
					flush($tmp);
				}
				
		        print "Operazione conclusa!<br /> <a href=\"$baselink?admin=sponsor&amp;\" title=\"Torna indietro\">Torna indietro</a><br /><br /><br /><br />";
		}
}
?>