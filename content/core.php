<?php
/* $Id: core.php,v 0.0.0.3 07/06/2006 02:02:07 mdb Exp $
 * $Author: mdb $
 *
 * www.be-you.org Core Scripts
 *
 * Copyright Kimera Team (c) 2006
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

/* Funzioni di "core" */
class BEYOU_CORE
{
		/* Page Rendering Timing */
		function getmicrotime()
		{
		         list($usec, $sec) = explode(" ",microtime());
		         return ((float)$usec + (float)$sec);
		}

		function formattext($text)
		{
				$return = str_replace("<", "[", str_replace(">", "]", $text));
				return $return;
		}

		function formattextrev($text)
		{
				$return = str_replace("[", "<", str_replace("]", ">", $text));
				return $return;
		}
		
		function getPagerData($numHits, $limit, $page) 
		{ 
				$numHits = (int) $numHits; 
				$limit = max((int) $limit, 1); 
				$page = (int) $page; 
				$numPages = ceil($numHits / $limit); 

				$page = max($page, 1); 
				$page = min($page, $numPages); 

				$offset = ($page - 1) * $limit; 

				$ret = new stdClass; 

				$ret->offset = $offset; 
				$ret->limit = $limit; 
				$ret->numPages = $numPages; 
				$ret->page = $page; 

				return $ret; 
		} 
		
		function paging($result)
		{
				global $pagina;
				if (empty($pagina)) $pagina = "1";
				$limit = 7; 
				$total = mysql_num_rows($result);

				$pager = BEYOU_CORE::getPagerData($total, $limit, $pagina); 
				$offset = $pager->offset; 
				$limit = $pager->limit; 
				$pagina = $pager->page;

				print "<table>";

				if ($pagina == 1) 
					echo "<tr><td>Precedente</td><td>"; 
				else 
					echo "<tr><td><a href=\"" . str_replace("&page=$pagina", "", $_SERVER[HTTP_X_REWRITE_URL]) . "&amp;pagina=" . ($pagina - 1) . "\">Precedente</a></td><td>";

				for ($i = 1; $i <= $pager->numPages; $i++) { 
						echo " "; 
						if ($i == $pager->page) 
						echo "<b>$i</b>"; 
						else 
						echo "<a href=\"" . str_replace("&pagina=$pagina", "", $_SERVER[HTTP_X_REWRITE_URL]) . "&amp;pagina=$i\">$i</a>"; 
				}

				if ($pagina == $pager->numPages) 
					echo "</td><td>Successiva</td></tr></table>"; 
				else 
					echo "</td><td><a href=\"" . str_replace("&pagina=$pagina", "", $_SERVER[HTTP_X_REWRITE_URL]) . "&amp;pagina=" . ($pagina + 1) . "\">Successiva</a></td></tr></table>"; 
					
				return $offset.",".$limit;
		}

		function rm($fileglob)
		{
		   if (is_string($fileglob)) {
		       if (is_file($fileglob)) {
		           return unlink($fileglob);
		       } else if (is_dir($fileglob)) {
		           $ok = BEYOU_CORE::rm("$fileglob/*");
		           if (! $ok) {
		               return false;
		           }
		           return rmdir($fileglob);
		       } else {
		           $matching = glob($fileglob);
		           if ($matching === false) {
		               trigger_error(sprintf('No files match supplied glob %s', $fileglob), E_USER_WARNING);
		               return false;
		           }     
		           $rcs = array_map('rm', $matching);
		           if (in_array(false, $rcs)) {
		               return false;
		           }
		       }     
		   } else if (is_array($fileglob)) {
		       $rcs = array_map('rm', $fileglob);
		       if (in_array(false, $rcs)) {
		           return false;
		       }
		   } else {
		       trigger_error('Param #1 must be filename or glob pattern, or array of filenames or glob patterns', E_USER_ERROR);
		       return false;
		   }

		   return true;
		}
		function is_image($file)
		{
		 		 $fltmp = explode('.', $file);
		 		 $tipo = array_pop($fltmp);
		 		 if ( strtolower($tipo) != "jpeg" && strtolower($tipo) != "jpg" && strtolower($tipo) != "gif" && strtolower($tipo) != "bmp" && strtolower($tipo) != "png") {
		 		 		return 0; 		 
				 } else {
		 		   		return 1;
		 		 }
		}
}
?>